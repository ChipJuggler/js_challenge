const localNum = 39;

const testObject = {
  a: 1,
  b: 'someString',
  c: /\d+/,
  d: (num) => `result: ${num + localNum}`,

  // higher-order functions:
  // there are several ways to implement it, but the main question is:
  // should we store results here and for each sequential call we'll
  // use what we stored before
  // or
  // store parameters chain on a client and run the whole thing with them
  //
  // the results could be completely different, because local vars can change
  // between the calls
  e: (num1) => (num2) => (num3) => `result: ${num1 + num2 + num3 + localNum}`,
  f: BigInt(Number.MAX_SAFE_INTEGER) + BigInt(Number.MAX_SAFE_INTEGER),

  // Symbols
  // I'm not sure why Symbol should be a part of it.
  // What is the expected behaviour? Transfer symbol key and create a
  // (probably global) symbol on the other side?
  // or it's about getting something from the object by symbol (obj[symbol])
  // and executing it (if needed)?
};

const shareObject = (name, obj) => {
  const bc = new BroadcastChannel(name);

  bc.onmessage = (ev) => {
    // console.log(ev);
    const { ping, name: remoteName, key, params, requestId, calls } = ev.data;

    if (ping && remoteName === name) {
      bc.postMessage({
        pong: true,
        name,
        response: Object.keys(obj).reduce(
          (res, k) => ({ ...res, [k]: typeof obj[k] }),
          {},
        ),
      });
    }

    if (key && requestId) {
      if (!obj[key]) {
        bc.postMessage({ key, requestId, error: `No such key: ${key}` });
        return;
      }
      if (typeof obj[key] === 'function') {
        // HACK: params array is used to do multiple calls,
        // separate parameter can be used
        let response;
        if (Array.isArray(params)) {
          console.log(params)
          // FIXME: deepClone params and modify clone to prevent funky bugs in the future
          response = params.reduce(
            (res, v) => (typeof res === 'function' ? res(v) : res),
            obj[key](params.splice(0, 1)[0]),
          );
        } else {
          response = obj[key](params);
        }
        if (typeof response === 'function') {
          bc.postMessage({ key, requestId, type: 'function' });
        } else {
          bc.postMessage({ key, requestId, response });
        }
      } else if (typeof obj[key] === 'bigint') {
        bc.postMessage({
          key,
          requestId,
          response: obj[key].toString(),
          type: 'bigint',
        });
      } else {
        bc.postMessage({ key, requestId, response: obj[key] });
      }
    }
  };

  return {
    _close: bc.close,
  };
};

shareObject('shared_object', testObject);
