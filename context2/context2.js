const TIMEOUT = 5000;

const generateProperty = (name, key, prevParams) => (params) => {
  const bc = new BroadcastChannel(name);

  return Promise.race([
    new Promise((resolve, reject) => {
      const requestId = new Date().getTime();
      bc.onmessage = (ev) => {
        const {
          key: remoteKey,
          response,
          type,
          requestId: remoteRequestId,
          error,
        } = ev.data;

        if (remoteKey === key && remoteRequestId === requestId) {
          bc.close();
          if (error) {
            reject(error);
          } else if (type === 'function') {
            resolve(
              generateProperty(
                name,
                key,
                prevParams ? [...prevParams, params] : [params],
              ),
            );
          } else if (type === 'bigint') {
            resolve(BigInt(response));
          } else {
            resolve(response);
          }
        }
      };

      // "remember" previous params for nested func calls and use
      // a "hack"/array to replicate the whole call
      if (prevParams) {
        bc.postMessage({
          key: key,
          params: [...prevParams, params],
          requestId,
        });
      } else {
        bc.postMessage({ key: key, params, requestId });
      }
    }),
    new Promise((_, reject) =>
      setTimeout(() => {
        bc.close();
        reject();
      }, TIMEOUT),
    ),
  ]);
};

// TODO: wrap with Proxy (if needed), so it will behave more like object
// Proxy will allow creation of an object with dynamic remote fields
// and probably it's a more elegant solution
const connectToObjectNoProxy = async (name) => {
  const bc = new BroadcastChannel(name);

  return Promise.race([
    new Promise((resolve) => {
      bc.onmessage = (ev) => {
        // console.log(ev);

        if (ev.data.pong && ev.data.name === name) {
          console.log(ev.data.response);
          resolve({
            ...Object.fromEntries(
              Object.entries(ev.data.response).map(([key, type]) => [
                key,
                type === 'function'
                  ? generateProperty(name, key)
                  : generateProperty(name, key)(),
              ]),
            ),
          });
          bc.close();
        }
      };
      bc.postMessage({ ping: true, name });
    }),
    new Promise((_, reject) =>
      setTimeout(() => {
        bc.close();
        reject();
      }, TIMEOUT),
    ),
  ]);
};

(async () => {
  let remoteObj = null;
  try {
    remoteObj = await connectToObjectNoProxy('shared_object');
  } catch {
    console.log('remote "shared_object" is not available');
  }

  if (!remoteObj) return;

  try {
    console.group('all props enum');
    for (let key in remoteObj) {
      try {
        // functions
        console.log(await remoteObj[key](1));
      } catch {
        // properties
        console.log(await remoteObj[key]);
      }
    }
    console.groupEnd();

    console.group('recursive func calls');
    const s1 = await remoteObj.e(1);
    const s2 = await s1(1);
    const s3 = await s2(1);
    console.log(s3);

    const f1 = await remoteObj.e(1);
    const f2 = await f1();
    const f3 = await f2(1);
    // will be NaN because of [1, undefined, 1]
    console.log(f3);
    console.groupEnd();
  } catch (e) {
    console.log(e);
  }
})();
